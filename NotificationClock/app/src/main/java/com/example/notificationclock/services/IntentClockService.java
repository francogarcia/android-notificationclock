package com.example.notificationclock.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.os.IBinder;
import android.os.Process;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.notificationclock.IntentClockServiceActivity;
import com.example.notificationclock.R;
import com.example.notificationclock.util.Utilities;

//
public class IntentClockService extends Service {
    private final class ServiceHandler extends Handler {
        // TODO Thread-safety.
        private boolean isRunning = false;

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            isRunning = true;
            while (isRunning) {
                notificationBuilder.setContentText(Utilities.buildNotificationString(timeElapsedSeconds));
                notification = notificationBuilder.build();
                NotificationManagerCompat notificationManager =
                        NotificationManagerCompat.from(IntentClockService.this);
                notificationManager.notify(notificationId, notification);

                Log.d("TAG", Utilities.buildNotificationString(timeElapsedSeconds));

                ++timeElapsedSeconds;

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
            }

            stopSelf(msg.arg1);
        }

        public boolean isRunning() {
            return isRunning;
        }

        public void stop() {
            isRunning = false;
        }
    }

    static final String CHANNEL_ID = "INTENT_CLOCK_SERVICE_CHANNEL";
    static final String HANDLER_THREAD_NAME = "IntentClockService Handler Thread";
    // Must not be 0 (<https://developer.android.com/guide/components/services#java>).
    static final int FOREGROUND_ID = 1;

    private Looper serviceLooper = null;
    private ServiceHandler serviceHandler = null;

    private int notificationId = 1;
    private NotificationCompat.Builder notificationBuilder = null;
    private Notification notification = null;

    private int timeElapsedSeconds = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        HandlerThread thread = new HandlerThread(
                HANDLER_THREAD_NAME,
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);

        createNotificationChannel();

        Intent notificationIntent = new Intent(this, IntentClockServiceActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_access_time_black_24dp)
                .setContentTitle(getResources().getString(R.string.time_now))
                .setContentText(Utilities.buildNotificationString(timeElapsedSeconds))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setAutoCancel(false);
        notification = notificationBuilder.build();
        // notification.flags |= Notification.FLAG_NO_CLEAR;
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, notification);
    }

    @Override
    public void onDestroy() {
        stopIntentClockService();

        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Message message = serviceHandler.obtainMessage();
        message.arg1 = startId;
        serviceHandler.sendMessage(message);

        // <https://developer.android.com/guide/components/services#java>
        // However, because you handle each call to onStartCommand() yourself,
        // you can perform multiple requests simultaneously. That's not what this
        // example does, but if that's what you want, you can create a new thread
        // for each request and run them right away instead of waiting for the
        // previous request to finish.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // <https://developer.android.com/guide/components/services#java>
            // If your app targets API level 26 or higher, the system imposes
            // restrictions on using or creating background services unless the app
            // itself is in the foreground.
            // (...)
            // Once the service has been created, the service must call its
            // startForeground() method within five seconds.
            notificationBuilder.setContentText(Utilities.buildNotificationString(timeElapsedSeconds));
            notification = notificationBuilder.build();
            // TODO Use a UUID?
            startForeground(FOREGROUND_ID, notification);
        }

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // According to <https://developer.android.com/training/notify-user/channels.html>,
    // API level 26+ requires that all notifications must be assigned to a channel.
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void stopIntentClockService() {
        serviceHandler.stop();

        stopSelf();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            stopForeground(true);
        } else {
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(IntentClockService.this);
            notificationManager.cancel(notificationId);
        }
    }

    public int getTimeElapsedSeconds() {
        return timeElapsedSeconds;
    }
}
