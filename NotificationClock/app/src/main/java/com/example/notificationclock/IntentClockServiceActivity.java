package com.example.notificationclock;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ToggleButton;

import com.example.notificationclock.services.IntentClockService;
import com.example.notificationclock.util.Utilities;

public class IntentClockServiceActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ToggleButton toggleButton = findViewById(R.id.toggleButton);
        toggleButton.setChecked(Utilities.isServiceRunning(this, IntentClockService.class.getName()));

        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (toggleButton.isChecked()) {
                    Intent intent = new Intent(IntentClockServiceActivity.this, IntentClockService.class);
                    ContextCompat.startForegroundService(IntentClockServiceActivity.this, intent);
                } else {
                    Intent intent = new Intent(IntentClockServiceActivity.this, IntentClockService.class);
                    stopService(intent);
                }
            }
        });
    }
}
