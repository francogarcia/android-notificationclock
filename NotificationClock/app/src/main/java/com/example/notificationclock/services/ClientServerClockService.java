package com.example.notificationclock.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Process;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.notificationclock.ClientServerClockServiceActivity;
import com.example.notificationclock.R;
import com.example.notificationclock.util.Utilities;

//
public class ClientServerClockService extends Service {
    public class ClientServerClockServiceBinder extends Binder {
        public ClientServerClockService getClientServerClockService() {
            return ClientServerClockService.this;
        }
    }

    private final class ServiceHandler extends Handler {
        // TODO Thread-safety.
        private boolean isRunning = false;

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            isRunning = true;
            while (isRunning) {
                notificationBuilder.setContentText(Utilities.buildNotificationString(timeElapsedSeconds));
                notification = notificationBuilder.build();
                NotificationManagerCompat notificationManager =
                        NotificationManagerCompat.from(ClientServerClockService.this);
                notificationManager.notify(notificationId, notification);

                Log.d("TAG", Utilities.buildNotificationString(timeElapsedSeconds));

                ++timeElapsedSeconds;

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
            }

            stopSelf(msg.arg1);

            Toast.makeText(ClientServerClockService.this, getString(R.string.job_finished), Toast.LENGTH_SHORT).show();
        }

        public boolean isRunning() {
            return isRunning;
        }

        public void stop() {
            isRunning = false;
        }
    }

    static final String CHANNEL_ID = "CLIENT_SERVER_CLOCK_SERVICE_CHANNEL";
    static final String HANDLER_THREAD_NAME = "ClientServerClockService Handler Thread";
    static final int NO_CONNECTION = -1;
    // Must not be 0 (<https://developer.android.com/guide/components/services#java>).
    static final int FOREGROUND_ID = 1;

    private Looper serviceLooper = null;
    private ServiceHandler serviceHandler = null;

    private int connectionId = NO_CONNECTION;
    // private ServiceConnection serviceConnection = null;

    private final IBinder binder = new ClientServerClockServiceBinder();

    private int notificationId = 1;
    private NotificationCompat.Builder notificationBuilder = null;
    private Notification notification = null;

    private int timeElapsedSeconds = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        HandlerThread thread = new HandlerThread(
                HANDLER_THREAD_NAME,
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);

        createNotificationChannel();

        Intent notificationIntent = new Intent(this, ClientServerClockServiceActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_access_time_black_24dp)
                .setContentTitle(getResources().getString(R.string.time_now))
                .setContentText(Utilities.buildNotificationString(timeElapsedSeconds))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setAutoCancel(false);
        notification = notificationBuilder.build();
        // notification.flags |= Notification.FLAG_NO_CLEAR;
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(notificationId, notification);
    }

    @Override
    public void onDestroy() {
        if (connectionId != NO_CONNECTION) {
            // Toast.makeText(ClientServerClockService.this, "---> onDestroy(): no connection.", Toast.LENGTH_SHORT).show();
            // unbindService(serviceConnection);
            stopClientServerClockService();
        }

        Toast.makeText(ClientServerClockService.this, getString(R.string.service_finished), Toast.LENGTH_SHORT).show();

        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, getString(R.string.job_started), Toast.LENGTH_SHORT).show();

        Message message = serviceHandler.obtainMessage();
        message.arg1 = startId;
        serviceHandler.sendMessage(message);

        // TODO Could bind it here!
        connectionId = startId;

        // <https://developer.android.com/guide/components/services#java>
        // However, because you handle each call to onStartCommand() yourself,
        // you can perform multiple requests simultaneously. That's not what this
        // example does, but if that's what you want, you can create a new thread
        // for each request and run them right away instead of waiting for the
        // previous request to finish.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // <https://developer.android.com/guide/components/services#java>
            // If your app targets API level 26 or higher, the system imposes
            // restrictions on using or creating background services unless the app
            // itself is in the foreground.
            // (...)
            // Once the service has been created, the service must call its
            // startForeground() method within five seconds.
            notificationBuilder.setContentText(Utilities.buildNotificationString(timeElapsedSeconds));
            notification = notificationBuilder.build();
            // TODO Use a UUID?
            startForeground(FOREGROUND_ID, notification);
        }

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Could register the client connection here.
        // If we do so, we may also implement onUnbind().

        // TODO Should define a criteria for the keys.
        if (connectionId == NO_CONNECTION) {
            // Toast.makeText(this, "---> onBind(): no connection.", Toast.LENGTH_LONG).show();
            connectionId = 1;
            Message message = serviceHandler.obtainMessage();
            message.arg1 = connectionId;
            serviceHandler.sendMessage(message);
        }

        Toast.makeText(this, getString(R.string.activity_connected), Toast.LENGTH_SHORT).show();

        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // serviceHandler.stop();
        // connectionId = NO_CONNECTION;
        // serviceConnection = null;

        // stopClientServerClockService();

        Toast.makeText(this, getString(R.string.activity_disconnected), Toast.LENGTH_SHORT).show();

        super.onUnbind(intent);

        return false;
    }

    // According to <https://developer.android.com/training/notify-user/channels.html>,
    // API level 26+ requires that all notifications must be assigned to a channel.
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void stopClientServerClockService() {
        serviceHandler.stop();

        connectionId = NO_CONNECTION;
        // stopSelf(connectionId);
//        if (serviceConnection != null) {
//            unbindService(serviceConnection);
//            serviceConnection = null;
//            connectionId = NO_CONNECTION;
//        }

        stopSelf();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            stopForeground(true);
        } else {
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(ClientServerClockService.this);
            notificationManager.cancel(notificationId);
        }
    }

//    public void setServiceConnection(ServiceConnection serviceConnection) {
//        this.serviceConnection = serviceConnection;
//    }
//
//    public ServiceConnection getServiceConnection() {
//        return serviceConnection;
//    }

    public int getTimeElapsedSeconds() {
        return timeElapsedSeconds;
    }
}
