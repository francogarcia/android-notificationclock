package com.example.notificationclock;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.notificationclock.services.ClientServerClockService;
import com.example.notificationclock.util.Utilities;

public class ClientServerClockServiceActivity extends AppCompatActivity {
    private ClientServerClockService clientServerClockService;
    private ServiceConnection clientServerClockServiceConnection;

    ToggleButton toggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean isClientServerClockServiceRunning = Utilities.isServiceRunning(this, ClientServerClockService.class.getName());
        if (isClientServerClockServiceRunning) {
            createConnection();
        } else {
            clientServerClockServiceConnection = null;
        }

        toggleButton = findViewById(R.id.toggleButton);
        toggleButton.setChecked(isClientServerClockServiceRunning);
        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (toggleButton.isChecked()) {
                    // createConnection();
                    Intent intent = new Intent(ClientServerClockServiceActivity.this, ClientServerClockService.class);
                    ContextCompat.startForegroundService(ClientServerClockServiceActivity.this, intent);
                } else {
                    // TODO Could use the server to unregister everything.
                    if (clientServerClockService != null) {
                        clientServerClockService.stopClientServerClockService();
                    }

                    if (clientServerClockServiceConnection != null) {
                        // Activity was restored.
                        clientServerClockService.stopClientServerClockService();
                        unbindService(clientServerClockServiceConnection);
                    }

                    Intent intent = new Intent(ClientServerClockServiceActivity.this, ClientServerClockService.class);
                    stopService(intent);

                    clientServerClockServiceConnection = null;
                    clientServerClockService = null;
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (clientServerClockServiceConnection != null) {
            unbindService(clientServerClockServiceConnection);
        }

        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void createConnection() {
        clientServerClockServiceConnection = new ServiceConnection() {
            ClientServerClockService.ClientServerClockServiceBinder clientServerClockServiceBinder;

            @Override
            public void onServiceConnected(ComponentName componentName, IBinder service) {
                clientServerClockServiceBinder = (ClientServerClockService.ClientServerClockServiceBinder) service;
                clientServerClockService = getClientServerClockService();
                // clientServerClockService.setServiceConnection(this);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                getClientServerClockService().stopClientServerClockService();

                clientServerClockService = null;
            }

            private ClientServerClockService getClientServerClockService() {
                return clientServerClockServiceBinder.getClientServerClockService();
            }
        };

        Intent intent = new Intent(this, ClientServerClockService.class);
        bindService(intent, clientServerClockServiceConnection, Context.BIND_AUTO_CREATE);
    }
}
