package com.example.notificationclock.util;

import android.app.ActivityManager;
import android.content.Context;
import android.text.format.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilities {
    public static String buildNotificationString(int elapsedTimeSeconds) {
        return (getCurrentTimeString());
    }

    public static String getCurrentTimeString() {
        // TODO Account to timezone.
        return new SimpleDateFormat("HH:mm:ss").format(new Date());
    }

    public static String secondsToTimeString(int seconds) {
        return DateUtils.formatElapsedTime(seconds);
    }

    public static boolean isServiceRunning(Context context, String serviceName) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceName.equals(service.service.getClassName())) {

                return true;
            }
        }

        return false;
    }
}
